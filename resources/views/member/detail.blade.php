@extends('layout/main')
@section('menu-bootcamp', 'active')
@section('menu-title', $isEdit ? 'Members Edit' : 'Members Detail')
@section('item-route', $isEdit ? 'Edit' : 'Detail')
@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ $isEdit ? 'Edit' : 'Detail' }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ url('/member-edit-proces') }}">
            @csrf
            <div class="card-body">
                <input type="hidden" class="form-control" name="id" id="id" value="{{ $data->id }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input @disabled(!$isEdit) type="text" class="form-control" name="name" id="name"
                        value="{{ $data->name }}">
                </div>
                <div class="form-group">
                    <label for="univ">Universitas</label>
                    <input @disabled(!$isEdit) type="text" class="form-control" name="univ" id="univ"
                        value="{{ $data->univ }}">
                </div>
                <div class="form-group">
                    <label for="asal">Asal</label>
                    <input @disabled(!$isEdit) type="text" class="form-control" name="asal" id="asal"
                        value="{{ $data->asal }}">
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a href="/member" class="btn btn-danger">Back</a>
                @if ($isEdit)
                    <button type="submit" class="btn btn-primary">Edit</button>
                @endif
            </div>
        </form>
    </div>
@endsection
