@extends('layout/main')
@section('menu-bootcamp', 'active')
@section('menu-title', 'Bootcamp Batch 9')
@section('item-route', 'Bootcamp')
@section('content')
    <div>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Daftar peserta bootcamp batch 9</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">No</th>
                            <th>Nama</th>
                            <th>Universitas</th>
                            <th>Asal</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $member)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $member['name'] }}</td>
                                <td class="text-{{ $color[$member['univ']] }}">{{ $member['univ'] }}</td>
                                <td>{{ $member['asal'] }}</td>
                                <td>
                                    <a href="{{ url('/member-detail/' . $member->id) }}" class="btn btn-primary">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ url('/member-edit/' . $member->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ url('/member-delete/' . $member->id) }}" class="btn btn-danger">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>

@endsection
