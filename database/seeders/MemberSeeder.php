<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Member;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Ryzal', 'univ' => 'UPN Veteran Yogyakarta', 'asal' => 'Sumedang'],
            ['name' => 'Christoper', 'univ' => 'Atma Jaya', 'asal' => 'Tegal'],
            ['name' => 'Fatturohman', 'univ' => 'UPN Veteran Yogyakarta', 'asal' => 'Yogyakarta'],
            ['name' => 'Samuel', 'univ' => 'UKDW', 'asal' => 'Purworejo'],
            ['name' => 'Edwin', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
            ['name' => 'Kiki', 'univ' => 'UKDW', 'asal' => 'Yogyakarta'],
            ['name' => 'Nuel', 'univ' => 'UKDW', 'asal' => 'Tegal'],
            ['name' => 'Novianto', 'univ' => 'Atma Jaya', 'asal' => 'Yogyakarta'],
            ['name' => 'Bagas', 'univ' => 'Atma Jaya', 'asal' => 'Sulawesi'],
            ['name' => 'Stevanus', 'univ' => 'Atma Jaya', 'asal' => 'Solo']
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
