<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = [
        //     ['nama' => 'Ryzal', 'universitas' => 'UPN Veteran Yogyakarta', 'asal' => 'Sumedang'],
        //     ['nama' => 'Christoper', 'universitas' => 'Atma Jaya', 'asal' => 'Tegal'],
        //     ['nama' => 'Fatturohman', 'universitas' => 'UPN Veteran Yogyakarta', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Samuel', 'universitas' => 'UKDW', 'asal' => 'Purworejo'],
        //     ['nama' => 'Edwin', 'universitas' => 'UKDW', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Kiki', 'universitas' => 'UKDW', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Nuel', 'universitas' => 'UKDW', 'asal' => 'Tegal'],
        //     ['nama' => 'Novianto', 'universitas' => 'Atma Jaya', 'asal' => 'Yogyakarta'],
        //     ['nama' => 'Bagas', 'universitas' => 'Atma Jaya', 'asal' => 'Sulawesi'],
        //     ['nama' => 'Stevanus', 'universitas' => 'Atma Jaya', 'asal' => 'Solo']
        // ];

        $color = [
            'Atma Jaya' => 'success',
            'UKDW' => 'primary',
            'UPN Veteran Yogyakarta' => 'danger',
        ];

        $data = Member::all();

        return View('member.index', ['data' => $data, 'color' => $color]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = Member::find($id);
        // dd($details);
        return View('member.detail', ['data' => $details,  'isEdit' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = Member::find($id);
        return View('member.detail', ['data' => $details, 'isEdit' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateData = Member::find($request->id);

        $updateData->id = $request->id;
        $updateData->name = $request->name;
        $updateData->univ = $request->univ;
        $updateData->asal = $request->asal;
        // $updateData->save();

        if ($updateData->save()) {
            return redirect('/member');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $member = Member::destroy($id);
        return redirect('/member');
    }
}
